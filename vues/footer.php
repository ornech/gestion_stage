<?php
require_once 'config/auth.php';
?>

</main>
<br>
  <footer class="text-body-secondary border-top">
    Created by Noboby &#127279; 2023
    <BR><i class="fa-brands fa-square-git"></i><a href="https://github.com/ornech/gestion_stage">dépôt GitHub</a>

  </footer>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

</body>
</html>
