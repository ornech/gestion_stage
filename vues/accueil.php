<!-- accueil.php -->
<?php
require_once 'config/auth.php';
//include 'vues/vue_activite_bouton.php';
?>
<BR>

<div class="alert alert-danger" role="alert">
  <h4 class="alert-heading"><i class="fa-solid fa-triangle-exclamation"></i> BETA 0.1.1</h4>
  <p>Cette application est une version beta. Elle est en aucun cas prêt pour une mise en production.</p>
  <hr>
  <p class="mb-0">Cependant l'outil "Recherche" est fonctionnel. Vous pouvez l'utiliser pour faire vos investigations sur les entreprises suceptibles de vous prendre en stage.</p>
</div>
</div>
